"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến toàn cục để lưu trữ id album đang đc update or delete. Mặc định = 0;
var gAlbumId = 0;
var gAlbumArr = [];
var gImageArr = [];

//const gBASE_URL = "http://localhost:8080/albums";
const gCONTENT_TYPE = "application/json;charset=UTF-8";

// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gALBUM_COLS = ["id", "lastName", "firstName", "phoneNumber", "city", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gALBUM_ID_COL = 0;
const gALBUM_CODE_COL = 1;
const gALBUM_NAME_COL = 2;
const gALBUM_NOTE_COL = 3;
const gALBUM_CREATE_COL = 4;
const gALBUM_ACTION_COL = 5;

// Biến toàn cục để hiển lưu STT
var gSTT = 1;
// Khai báo DataTable & mapping collumns
var gAlbumTable = $("#example1").DataTable({
  columns: [
    { data: gALBUM_COLS[gALBUM_ID_COL] },
    { data: gALBUM_COLS[gALBUM_CODE_COL] },
    { data: gALBUM_COLS[gALBUM_NAME_COL] },
    { data: gALBUM_COLS[gALBUM_NOTE_COL] },
    { data: gALBUM_COLS[gALBUM_CREATE_COL] },
    { data: gALBUM_COLS[gALBUM_ACTION_COL] }
  ],
  columnDefs: [
    { // định nghĩa lại cột action
      targets: gALBUM_ACTION_COL,
      defaultContent: `
        <button id= "album-detail"  <i title = 'Detail' class="fa-solid fa-circle-info" style = "cursor:pointer;"></i></button> &nbsp; 
        <button id= "album-edit" <i title = 'Edit' class="fa-solid fa-pen-to-square" style = "cursor:pointer;"></i></button> &nbsp;
        <button id= "album-delete" <i title = 'Delete' class="fa-solid fa-trash" style = "cursor:pointer;"></i></button>
    `
    }
  ]
});

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  // thực hiện tải trang
  onPageLoading();

  // 1 - R: gán sự kiện Read - cho button album-detail trên grid
  $("#example1").on("click", "#album-detail", function () {
    onBtnDetailAlbumClick(this);
  });

  // 2 - C: gán sự kiện Create - Thêm mới album
  $("#btn-add-album").on("click", function () {
    onBtnAddNewAlbumClick();
  });

  
  // 3 - U: gán sự kiện Update - Sửa 1 album
  $("#example1").on("click", "#album-edit", function () {
    onBtnEditAlbumClick(this);
  });

  // 4 - D: gán sự kiện Delete - Xóa 1 album
  $("#example1").on("click", "#album-delete", function () {
    onBtnDeleteAlbumClick(this);
  });


  // gán sự kiện cho nút Create Album (trên modal)
  $("#btn-create-album").on("click", function () {
    onBtnCreateAlbumClick();
  });

  // gán sự kiện cho nút Update Album (trên modal)
  $("#btn-update-album").on("click", function () {
    onBtnUpdateAlbumClick();
  });

  // gán sự kiện cho nút Confirm trên modal delete
  $("#btn-confirm-delete-album").on("click", function () {
    onBtnConfirmDeleteClick();
  })

   // gán sự kiện cho nút Close trên modal detail
   $("#btn-detail-close").on("click", function () {
    resertDetailAlbumForm();
  })

  //gán sự kiện cho nút btn-all-images
  $("#btn-all-images").on("click", function () {
    window.location.href = "images.html";
  })
  
  
});




/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm thực thi khi trang được load
function onPageLoading() {
  // 1 - R: Read / Load album to DataTable
  getAllCustomers();
}



/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm gọi api để lấy all danh sách albums đã đăng ký
function getAllCustomers() {
  $.ajax({
    url: "http://localhost:8080/allCustomers",
    type: "GET",
    success: function (paramAlbums) {
      console.log(paramAlbums);
      debugger;
      loadDataToAlbumTable(paramAlbums);
      gAlbumArr = paramAlbums;
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
/** load album array to DataTable
 * in: album array
 * out: album table has data
 */
function loadDataToAlbumTable(paramAlbumArr) {
  gSTT = 1;
  gAlbumTable.clear();
  gAlbumTable.rows.add(paramAlbumArr);
  gAlbumTable.draw();
}

