"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến toàn cục để lưu trữ id album đang đc update or delete. Mặc định = 0;
var gImageId = 0;
var gImageArr = [];
var gAlbumId = 0;

const gBASE_URL = "http://localhost:8080/images";
const gCONTENT_TYPE = "application/json;charset=UTF-8";

// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gIMAGE_COLS = ["id", "imageCode", "imageName", "link", "note", "createdAt", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gIMAGE_ID_COL = 0;
const gIMAGE_CODE_COL = 1;
const gIMAGE_NAME_COL = 2;
const gIMAGE_LINK_COL = 3;
const gIMAGE_NOTE_COL = 4;
const gIMAGE_CREATE_COL = 5;
const gIMAGE_ACTION_COL = 6;

// Biến toàn cục để hiển lưu STT
var gSTT = 1;
// Khai báo DataTable & mapping collumns
var gImageTable = $("#image-table").DataTable({
  columns: [
    { data: gIMAGE_COLS[gIMAGE_ID_COL] },
    { data: gIMAGE_COLS[gIMAGE_CODE_COL] },
    { data: gIMAGE_COLS[gIMAGE_NAME_COL] },
    { data: gIMAGE_COLS[gIMAGE_LINK_COL] },
    { data: gIMAGE_COLS[gIMAGE_NOTE_COL] },
    { data: gIMAGE_COLS[gIMAGE_CREATE_COL] },
    { data: gIMAGE_COLS[gIMAGE_ACTION_COL] }
  ],
  columnDefs: [
    { // định nghĩa lại cột action
      targets: gIMAGE_ACTION_COL,
      defaultContent: `
        <button id= "image-edit" <i title = 'Edit' class="fa-solid fa-pen-to-square" style = "cursor:pointer;"></i></button> &nbsp;
        <button id= "image-delete" <i title = 'Delete' class="fa-solid fa-trash" style = "cursor:pointer;"></i></button>
    `
    }
  ]
});

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  // thực hiện tải trang
  onPageLoading();

  // 2 - C: gán sự kiện Create - Thêm mới image
  $("#btn-add-image").on("click", function () {
    onBtnAddNewImageClick();
  });


  // 3 - U: gán sự kiện Update - Sửa 1 image
  $("#image-table").on("click", "#image-edit", function () {
    onBtnEditImageClick(this);
  });

  // 4 - D: gán sự kiện Delete - Xóa 1 image
  $("#image-table").on("click", "#image-delete", function () {
    onBtnDeleteAlbumClick(this);
  });


  // gán sự kiện cho nút Create Image (trên modal)
  $("#btn-create-image").on("click", function () {
    if (checkSelectAlbum()) {
      onBtnCreateImageClick();
    }
  });

  // gán sự kiện cho nút Update Image (trên modal)
  $("#btn-update-image").on("click", function () {
    onBtnUpdateImageClick();
  });

  // gán sự kiện cho nút Confirm trên modal delete
  $("#btn-confirm-delete-image").on("click", function () {
    onBtnConfirmDeleteClick();
  })
});




/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm thực thi khi trang được load
function onPageLoading() {
  // 1 - R: Read / Load album to DataTable
  getAllImages();
  //gọi hàm lấy danh sách toàn bộ albums
  getAlbumList();
}

// Hàm xử lý sự kiện khi nút Thêm mới đc click
function onBtnAddNewImageClick() {
  // hiển thị modal trắng lên
  $("#create-image-modal").modal("show");
}

// Hàm xử lý sự kiện khi icon edit album trên bảng đc click
function onBtnEditImageClick(paramBtnEdit) {
  console.log("Edit button click");
  // lưu thông tin albumId đang được edit vào biến toàn cục
  gImageId = getImageIdFromButton(paramBtnEdit);
  // load dữ liệu vào các trường dữ liệu trong modal
  getImageById(gImageId);
}

// Hàm xử lý sự kiện khi icon Delete image trên bảng đc click
function onBtnDeleteAlbumClick(paramBtnDelete) {
  console.log("Delete button click");
  // lưu thông tin imageId đang được delete vào biến toàn cục
  gImageId = getImageIdFromButton(paramBtnDelete);
  // bật modal confirm lên
  $("#delete-confirm-modal").modal("show");
}


// hàm xử lý sự kiện create image modal click
function onBtnCreateImageClick() {
  // khai báo đối tượng chứa image data
  var vImageObj = {
    imageCode: "",
    imageName: "",
    link: "",
    note: "Nothing",
  };
  // B1: Thu thập dữ liệu
  getCreateImageData(vImageObj);
  // B2: Validate insert
  var vIsImageValidate = validateImageData(vImageObj);
  gAlbumId = $("#select-album").val();
  if (vIsImageValidate) {
    // B3: insert image by album id
    $.ajax({
      url: gBASE_URL + "/create/" + gAlbumId,
      type: "POST",
      contentType: gCONTENT_TYPE,
      data: JSON.stringify(vImageObj),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleInsertImageSuccess()
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện update album modal click
function onBtnUpdateImageClick() {
  // khai báo đối tượng chứa album data
  var vImageObj = {
    imageCode: "",
    imageName: "",
    link: "",
    note: "Nothing",
  };
  // B1: Thu thập dữ liệu
  getUpdateImageData(vImageObj);
  // B2: Validate update
  var vIsImageValidate = validateImageData(vImageObj);
  if (vIsImageValidate) {
    // B3: update album
    $.ajax({
      url: gBASE_URL + "/update/" + gImageId,
      type: "PUT",
      contentType: gCONTENT_TYPE,
      data: JSON.stringify(vImageObj),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleUpdateImageSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện confirm delete
function onBtnConfirmDeleteClick() {
  // b1: thu thập dữ liệu (ko có)
  // B2: validate dữ liệu (ko có)
  // b3: gọi api để xóa
  $.ajax({
    url: gBASE_URL + "/delete/" + gImageId,
    type: "DELETE",
    success: function (paramRes) {
      handleDeleteImageSuccess();
    },
    error: function (paramErr) {
      alert("Failed to delete");
      $("#delete-confirm-modal").modal("hide");
    }
  });
  // b4: xử lý hiển thị front-end
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm gọi API lấy toàn bộ danh sách album đã đăng kí
function getAlbumList() {
  $.ajax({
    url: "http://localhost:8080/albums",
    dataType: "json",
    type: "GET",
    success: function (paramAlbums) {
      console.log(paramAlbums);
      loadDataToAlbumSelect(paramAlbums);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
// hàm gọi api để lấy all danh sách hình ảnh đã đăng ký
function getAllImages() {
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    success: function (paramImages) {
      console.log(paramImages);
      loadDataToImageTable(paramImages);
      gImageArr = paramImages;
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
/** load image array to DataTable
 * in: image array
 * out: image table has data
 */
function loadDataToImageTable(paramImageArr) {
  gSTT = 1;
  gImageTable.clear();
  gImageTable.rows.add(paramImageArr);
  gImageTable.draw();
}

// hàm get album by id
function getImageById(paramImageId) {
  $.ajax({
    url: gBASE_URL + "/details/" + paramImageId,
    type: "GET",
    success: function (paramAlbum) {
      showImageDataToUpdateModal(paramAlbum);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#update-image-modal").modal("show");
}

// hàm show image obj lên modal
function showImageDataToUpdateModal(paramImage) {
  $("#input-update-image-code").val(paramImage.imageCode);
  $("#input-update-image-name").val(paramImage.imageName);
  $("#textarea-note-update").val(paramImage.note);
  $("#input-update-image-link").val(paramImage.link);
}




// hàm thu thập dữ liệu để create album
function getCreateImageData(paramImageObj) {
  paramImageObj.imageCode = $("#input-create-image-code").val().trim();
  paramImageObj.imageName = $("#input-create-image-name").val().trim();
  paramImageObj.link = $("#input-create-image-link").val().trim();
  paramImageObj.note = $("#textarea-note").val().trim();
}

// hàm thu thập dữ liệu để update album
function getUpdateImageData(paramImageObj) {
  paramImageObj.imageCode = $("#input-update-image-code").val().trim();
  paramImageObj.imageName = $("#input-update-image-name").val().trim();
  paramImageObj.link = $("#input-update-image-link").val().trim();
  paramImageObj.note = $("#textarea-note-update").val().trim();
}

// hàm validate data
function validateImageData(paramImageObj) {
  if (paramImageObj.imageCode === "") {
    alert("Please input image code");
    return false;
  }
  if (paramImageObj.imageName === "") {
    alert("Please input image name");
    return false;
  }

  for (let i = 0; i < gImageArr.length; i++) {
    if (gImageArr[i].imageCode === paramImageObj.imageCode && gImageArr[i].id != gImageId) {
      alert("Image code already exists");
      return false;
    }
  }
  return true;

}

//check đã chọn album chưa
gAlbumId = $("#select-album").val();
function checkSelectAlbum() {
  if (gAlbumId === 0) {
    alert("Please select Album");
    return false;
  }
  else {
    //gAlbumId = gAlbumId;
    return true;
  }
}

// hàm xử lý hiển thị front-end khi thêm album thành công
function handleInsertImageSuccess() {
  alert("Added Image successfully !");
  getAllImages();
  resertCreateImageForm();
  $("#create-image-modal").modal("hide");
}

// hàm xử lý hiển thị front-end khi sửa album thành công
function handleUpdateImageSuccess() {
  alert("Updated Image successfully!");
  getAllImages();
  resertUpdateImageForm();
  $("#update-image-modal").modal("hide");
}

// hàm xử lý hiển thị front-end khi xóa album thành công
function handleDeleteImageSuccess() {
  alert("Deleted Image successfully!");
  getAllImages();
  $("#delete-confirm-modal").modal("hide");
}


// hàm xóa trắng form create album
function resertCreateImageForm() {
  $("#input-create-image-code").val("");
  $("#input-create-image-name").val("");
  $("#textarea-note").val("");
  $("#input-create-image-link").val("");
}

// hàm xóa trắng form update album
function resertUpdateImageForm() {
  $("#input-update-image-code").val("");
  $("#input-update-image-name").val("");
  $("#input-update-image-link").val("");
  $("textarea-note-update").val("");
}

// hàm dựa vào button detail (edit or delete) xác định đc id album
function getImageIdFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vImageRowData = gImageTable.row(vTableRow).data();
  return vImageRowData.id;
}

//hàm tạo option cho select albums trên create modal
function loadDataToAlbumSelect(paramAlbum) {
  "use strict";
  for (var bI = 0; bI < paramAlbum.length; bI++) {
    $("#select-album").append($("<option>", {
      text: paramAlbum[bI].albumName,
      value: paramAlbum[bI].id
    }));
  }
}