"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến toàn cục để lưu trữ id album đang đc update or delete. Mặc định = 0;
var gAlbumId = 0;
var gAlbumArr = [];
var gImageArr = [];

const gBASE_URL = "http://localhost:8080/albums";
const gCONTENT_TYPE = "application/json;charset=UTF-8";

// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gALBUM_COLS = ["id", "albumCode", "albumName", "note", "createdAt", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gALBUM_ID_COL = 0;
const gALBUM_CODE_COL = 1;
const gALBUM_NAME_COL = 2;
const gALBUM_NOTE_COL = 3;
const gALBUM_CREATE_COL = 4;
const gALBUM_ACTION_COL = 5;

// Biến toàn cục để hiển lưu STT
var gSTT = 1;
// Khai báo DataTable & mapping collumns
var gAlbumTable = $("#album-table").DataTable({
  columns: [
    { data: gALBUM_COLS[gALBUM_ID_COL] },
    { data: gALBUM_COLS[gALBUM_CODE_COL] },
    { data: gALBUM_COLS[gALBUM_NAME_COL] },
    { data: gALBUM_COLS[gALBUM_NOTE_COL] },
    { data: gALBUM_COLS[gALBUM_CREATE_COL] },
    { data: gALBUM_COLS[gALBUM_ACTION_COL] }
  ],
  columnDefs: [
    { // định nghĩa lại cột action
      targets: gALBUM_ACTION_COL,
      defaultContent: `
        <button id= "album-detail"  <i title = 'Detail' class="fa-solid fa-circle-info" style = "cursor:pointer;"></i></button> &nbsp; 
        <button id= "album-edit" <i title = 'Edit' class="fa-solid fa-pen-to-square" style = "cursor:pointer;"></i></button> &nbsp;
        <button id= "album-delete" <i title = 'Delete' class="fa-solid fa-trash" style = "cursor:pointer;"></i></button>
    `
    }
  ]
});

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  // thực hiện tải trang
  onPageLoading();

  // 1 - R: gán sự kiện Read - cho button album-detail trên grid
  $("#album-table").on("click", "#album-detail", function () {
    onBtnDetailAlbumClick(this);
  });

  // 2 - C: gán sự kiện Create - Thêm mới album
  $("#btn-add-album").on("click", function () {
    onBtnAddNewAlbumClick();
  });

  
  // 3 - U: gán sự kiện Update - Sửa 1 album
  $("#album-table").on("click", "#album-edit", function () {
    onBtnEditAlbumClick(this);
  });

  // 4 - D: gán sự kiện Delete - Xóa 1 album
  $("#album-table").on("click", "#album-delete", function () {
    onBtnDeleteAlbumClick(this);
  });


  // gán sự kiện cho nút Create Album (trên modal)
  $("#btn-create-album").on("click", function () {
    onBtnCreateAlbumClick();
  });

  // gán sự kiện cho nút Update Album (trên modal)
  $("#btn-update-album").on("click", function () {
    onBtnUpdateAlbumClick();
  });

  // gán sự kiện cho nút Confirm trên modal delete
  $("#btn-confirm-delete-album").on("click", function () {
    onBtnConfirmDeleteClick();
  })

   // gán sự kiện cho nút Close trên modal detail
   $("#btn-detail-close").on("click", function () {
    resertDetailAlbumForm();
  })

  //gán sự kiện cho nút btn-all-images
  $("#btn-all-images").on("click", function () {
    window.location.href = "images.html";
  })
  
  
});




/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm thực thi khi trang được load
function onPageLoading() {
  // 1 - R: Read / Load album to DataTable
  getAllAlbums();
}

// Hàm xử lý sự kiện khi nút Thêm mới đc click
function onBtnAddNewAlbumClick() {
  // hiển thị modal trắng lên
  $("#create-album-modal").modal("show");
}

// Hàm xử lý sự kiện khi icon edit album trên bảng đc click
function onBtnEditAlbumClick(paramBtnEdit) {
  console.log("Edit button click");
  // lưu thông tin albumId đang được edit vào biến toàn cục
  gAlbumId = getAlbumIdFromButton(paramBtnEdit);
  // load dữ liệu vào các trường dữ liệu trong modal
  getAlbumById(gAlbumId);
}

// Hàm xử lý sự kiện khi icon Delete album trên bảng đc click
function onBtnDeleteAlbumClick(paramBtnDelete) {
  console.log("Delete button click");
  // lưu thông tin albumId đang được delete vào biến toàn cục
  gAlbumId = getAlbumIdFromButton(paramBtnDelete);
  // bật modal confirm lên
  $("#delete-confirm-modal").modal("show");
}

// Hàm xử lý sự kiện khi icon Detail album trên bảng đc click
function onBtnDetailAlbumClick(paramBtnDetail) {
  console.log("Detail button click");
  // lưu thông tin albumId đang được click detail vào biến toàn cục
  gAlbumId = getAlbumIdFromButton(paramBtnDetail);
  // bật modal confirm lên
  $("#album-detail-modal").modal("show");
  getDetailById(gAlbumId);
}



// hàm xử lý sự kiện create album modal click
function onBtnCreateAlbumClick() {
  // khai báo đối tượng chứa album data
  var vAlbumObj = {
    albumCode: "",
    albumName: "",
    note:"Nothing",
  };
  // B1: Thu thập dữ liệu
  getCreateAlbumData(vAlbumObj);
  // B2: Validate insert
  var vIsAlbumValidate = validateAlbumData(vAlbumObj);
  if (vIsAlbumValidate) {
    // B3: insert album
    $.ajax({
      url: gBASE_URL + "/create",
      type: "POST",
      contentType: gCONTENT_TYPE,
      data: JSON.stringify(vAlbumObj),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleInsertAlbumSuccess()
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện update album modal click
function onBtnUpdateAlbumClick() {
  // khai báo đối tượng chứa album data
  var vAlbumObj = {
    albumCode: "",
    albumName: "",
    note:"Nothing",
  };
  // B1: Thu thập dữ liệu
  getUpdateAlbumData(vAlbumObj);
  // B2: Validate update
  var vIsAlbumValidate = validateAlbumData(vAlbumObj);
  if (vIsAlbumValidate) {
    // B3: update album
    $.ajax({
      url: gBASE_URL + "/update/" + gAlbumId,
      type: "PUT",
      contentType: gCONTENT_TYPE,
      data: JSON.stringify(vAlbumObj),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleUpdateAlbumSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện confirm delete
function onBtnConfirmDeleteClick() {
  // b1: thu thập dữ liệu (ko có)
  // B2: validate dữ liệu (ko có)
  // b3: gọi api để xóa
  $.ajax({
    url: gBASE_URL + "/delete/"+ gAlbumId,
    type: "DELETE",
    success: function (paramRes) {
      handleDeleteAlbumSuccess();
    },
    error: function (paramErr) {
      alert("Failed to delete");
      $("#delete-confirm-modal").modal("hide");
    }
  });
  // b4: xử lý hiển thị front-end
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm gọi api để lấy all danh sách albums đã đăng ký
function getAllAlbums() {
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    success: function (paramAlbums) {
      console.log(paramAlbums);
      loadDataToAlbumTable(paramAlbums);
      gAlbumArr = paramAlbums;
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
/** load album array to DataTable
 * in: album array
 * out: album table has data
 */
function loadDataToAlbumTable(paramAlbumArr) {
  gSTT = 1;
  gAlbumTable.clear();
  gAlbumTable.rows.add(paramAlbumArr);
  gAlbumTable.draw();
}

// hàm get album by id
function getAlbumById(paramAlbumId) {
  $.ajax({
    url: gBASE_URL + "/details/" + paramAlbumId,
    type: "GET",
    success: function (paramAlbum) {
      showAlbumDataToUpdateModal(paramAlbum);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#update-album-modal").modal("show");
}

// hàm show album obj lên modal
function showAlbumDataToUpdateModal(paramAlbum) {
  $("#input-update-album-code").val(paramAlbum.albumCode);
  $("#input-update-album-name").val(paramAlbum.albumName);
  $("#textarea-note-update").val(paramAlbum.note);
}

// hàm get album detail by id
function getDetailById(paramAlbumId) {
  $.ajax({
    url: gBASE_URL + "/details/" + paramAlbumId,
    type: "GET",
    success: function (paramAlbum) {
      showAlbumDataToDetailModal(paramAlbum);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#album-detail-modal").modal("show");
}

// hàm show detail album obj lên modal
function showAlbumDataToDetailModal(paramAlbum) {
  $("#detail-album-code").val(paramAlbum.albumCode);
  $("#detail-album-name").val(paramAlbum.albumName);
  $("#detail-album-note").val(paramAlbum.note);
  $("#detail-create-date").val(paramAlbum.createdAt);
  $("#detail-album-images").val("");
  var paramAlbumCode = paramAlbum.albumCode;
  $.ajax({
    url: gBASE_URL + "/image?albumCode=" + paramAlbumCode,
    type: "GET",
    success: function (paramImage) {
      console.log(paramImage);
     let txt = "";
     for (var i = 0; i < paramImage.length; i++){
      txt = txt + paramImage[i].imageName + ", ";
      $("#detail-album-images").val(txt);
    }
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}



  /*function getDetailByAlbumCode(paramAlbumCode){
    $.ajax({
      url: gBASE_URL + "/image?albumCode=" + paramAlbumCode,
      type: "GET",
      success: function (paramImage) {
        console.log(paramImage);
        return paramImage;
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });

  }*/




// hàm thu thập dữ liệu để create album
function getCreateAlbumData(paramAlbumObj) {
  paramAlbumObj.albumCode = $("#input-create-album-code").val().trim();
  paramAlbumObj.albumName = $("#input-create-album-name").val().trim();
  paramAlbumObj.note = $("#textarea-note").val().trim();
}

// hàm thu thập dữ liệu để update album
function getUpdateAlbumData(paramAlbumObj) {
  paramAlbumObj.albumCode = $("#input-update-album-code").val().trim();
  paramAlbumObj.albumName = $("#input-update-album-name").val().trim();
  paramAlbumObj.note = $("#textarea-note-update").val().trim();
}

// hàm validate data
function validateAlbumData(paramAlbumObj) {
  if (paramAlbumObj.albumCode === "") {
    alert("Please input album code");
    return false;
  }
  if (paramAlbumObj.albumName === "") {
    alert("Please input album name");
    return false;
  }

  for (let i=0; i< gAlbumArr.length; i++){
    if (gAlbumArr[i].albumCode === paramAlbumObj.albumCode && gAlbumArr[i].id != gAlbumId){
      alert("Album code already exists");
      return false;
    }
  }
  return true;
}


// hàm xử lý hiển thị front-end khi thêm album thành công
function handleInsertAlbumSuccess() {
  alert("Added Album successfully !");
  getAllAlbums();
  resertCreateAlbumForm();
  $("#create-album-modal").modal("hide");
}

// hàm xử lý hiển thị front-end khi sửa album thành công
function handleUpdateAlbumSuccess() {
  alert("Updated Album successfully!");
  getAllAlbums();
  resertUpdateAlbumForm();
  $("#update-album-modal").modal("hide");
}

// hàm xử lý hiển thị front-end khi xóa album thành công
function handleDeleteAlbumSuccess() {
  alert("Deleted Album successfully!");
  getAllAlbums();
  $("#delete-confirm-modal").modal("hide");
}


// hàm xóa trắng form create album
function resertCreateAlbumForm() {
  $("#input-create-album-code").val("");
  $("#input-create-album-name").val("");
  $("#textarea-note").val("");
  $("input-create-date").val("");
}

// hàm xóa trắng form update album
function resertUpdateAlbumForm() {
  $("#input-update-album-code").val("");
  $("#input-update-album-name").val("");
  $("textarea-note-update").val("");
}

// hàm xóa trắng form detail album
function resertDetailAlbumForm() {
  $("#detail-album-code").val("");
  $("#detail-album-note").val("");
  $("#detail-album-images").val("");
  $("detail-create-date").val("");
}



// hàm dựa vào button detail (edit or delete) xác định đc id album
function getAlbumIdFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vAlbumRowData = gAlbumTable.row(vTableRow).data();
  return vAlbumRowData.id;
}